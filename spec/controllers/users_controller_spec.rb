require "rails_helper"

RSpec.feature "signing in" do
    background do
        User.create(email:'aravindcranjan.mec@gmail.com', password:'123',name:"Aravind")
    end

    scenario "Signing in with correct credentials" do
        visit "/"
        within("form") do
            fill_in 'email', with: "aravindcranjan.mec@gmail.com"
            fill_in 'password', with: "123"
            click_button('Log In')
        end
        expect(page).to have_content "Logged in sucessfully"
    end
    
    scenario "Signing in with incorrect credentials" do
        visit "/login"
        within("form") do
            fill_in 'email', with: "aravindcranjan@gmail.com"
            fill_in 'password', with: "123123"
            click_button("Log In")
        end
        expect(page).to have_content "Login"
    end
end

RSpec.feature "Log out" do
    background do
        User.create(email:'aravindcranjan.mec@gmail.com', password:'123',name:"Aravind")
        visit "/login"
        fill_in 'email', with: "aravindcranjan.mec@gmail.com"
        fill_in 'password', with: "123"
        click_button('Log In')
    end

    scenario "Route handling working" do
        visit "/"
        expect(page).to have_content "Logged in sucessfully"
    end

    scenario "clicking log out button routes to login page" do
        visit "/users/1"
        click_link("Sign Out")
        expect(page).to have_content "Login"
    end
end

RSpec.feature "Sign up" do
    background do
        User.create(email:'aravindcranjan.mec@gmail.com', password:'123',name:"Aravind")
    end

    scenario "Signup with some fields missing" do
        visit "/signup"
        fill_in 'email', with: "aravindcranjan.mec@gmail.com"
        fill_in 'password', with: "123"
        click_button('Sign Up')
        expect(page).to have_content "Sign Up"
    end

    scenario "Signup with same email id in database" do
        visit "/signup"
        fill_in 'email', with: "aravindcranjan.mec@gmail.com"
        fill_in 'password', with: "123"
        fill_in 'name',with: "Aravind"
        click_button('Sign Up')
        expect(page).to have_content "Sign Up"
    end

    scenario "Signup with correct credentials" do
        visit "/signup"
        fill_in 'email', with: "aravindcranjan54@gmail.com"
        fill_in 'password', with: "123"
        fill_in 'name',with: "Aravind"
        click_button('Sign Up')
        expect(page).to have_content "Login"
    end

end