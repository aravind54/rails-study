Rails.application.routes.draw do
  resources :users, only:[:new, :show, :create]
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
  get "signup", to: "users#new", as:"signup"
  get "login", to: "login#new", as: "login"
  get "logout", to: "login#destroy", as:"logout"

  post "login", to:"login#create", as: "signin"
  get "dashboard",to:"users#show", as: "dashboard"

  root to:"login#new"

end
