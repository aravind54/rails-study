class LoginController < ApplicationController
   
   def new
    if(session[:user_id])
        redirect_to controller: "users", action: "show", id: session[:user_id]
    end
   end

   def create
    user  = User.find_by_email(params[:email])
    if(user && user.authenticate(params[:password]))
        session[:user_id] = user.id
        redirect_to controller: "users", action:"show", id:user.id
    else
        redirect_to root_path
    end
   end

   def destroy
    reset_session
    #or session[user_id] = nil
    redirect_to root_path
   end
end
